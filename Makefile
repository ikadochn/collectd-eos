NAME=collectd-eos
VERSION=0.3.1
SOURCEPKGNAME=$(NAME)-$(VERSION)
SOURCEPKGFILE=$(SOURCEPKGNAME).tar.gz
RELEASE=1
NVR=$(NAME)-$(VERSION)-$(RELEASE)
GIT=git+ssh://git@gitlab.cern.ch:7999/ikadochn/$(NAME).git

rpm: sources
	mkdir -p ~/rpmbuild/SOURCES
	mv $(SOURCEPKGFILE) ~/rpmbuild/SOURCES
	rpmbuild -ba --nodeps --target noarch $(NAME).spec

sources:
	tar cvzf $(SOURCEPKGFILE) --exclude-vcs --transform 's,^,$(SOURCEPKGNAME)/,' *

clean:
	rm $(SOURCEPKGFILE)

scratch7:
	koji build collectd7 --nowait --scratch $(GIT)#$(shell git rev-parse HEAD)

scratch6:
	koji build collectd6 --nowait --scratch $(GIT)#$(shell git rev-parse HEAD)

scratch: scratch6 scratch7

build7:
	koji build collectd7 --nowait $(GIT)#$(shell git rev-parse HEAD)

build6:
	koji build collectd6 --nowait $(GIT)#$(shell git rev-parse HEAD)

build: build6 build7

tag7-qa:
	koji tag-build collectd7-qa $(NVR).el7
tag6-qa:
	koji tag-build collectd6-qa $(NVR).el6

tag-qa: tag6-qa tag7-qa
