from collections import defaultdict

import collectd
import os
import socket
import subprocess

# python 3 moved urlparse
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

PLUGIN_NAME = "eos"

CONFIG_DEFAULT_MGM_URL = None  # use $EOS_MGM_URL or current host
CONFIG_DEFAULT_INTERVAL = -1  # use collectd default
CONFIG_DEFAULT_EOS_CLIENT_COMMAND = ("eos",)
DEBUG = False


def _debug(*args, **kwargs):
    if DEBUG:
        collectd.debug(*args, **kwargs)


def _parse_em_line(line):
    _debug("Plugin {plugin}: parsing metrics line {line!r}".format(
        plugin=PLUGIN_NAME, line=line))
    return dict([key_val.split("=") for key_val in line.split()])


def _eos_command(mgm_url, command, eos_client_command):
    _debug("Plugin {plugin}: running eos client command {full_command!r}".format(
        plugin=PLUGIN_NAME, full_command=" ".join(eos_client_command + ("-b", "-r", "0", "0", mgm_url, command))))
    process = subprocess.Popen(eos_client_command + ("-b", "-r", "0", "0", mgm_url, command),
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    if stderr:
        collectd.error("Plugin {plugin}: error when running command {command!r}:\n{stderr}".format(
                       plugin=PLUGIN_NAME, command=command, stderr=stderr))
    try:
        stdout = stdout.decode("utf-8")  # decode bytes in python3 only
    except AttributeError:
        pass
    return [line for line in stdout.split("\n") if line.strip()]  # return stdout lines that are not whitespace only


def _type_cast(value, T, min_value=None):
    if value.endswith("%"):
        value = value[:-1]
    try:
        return T(value)
    except ValueError:
        return min_value - 1 if min_value else -1


def _extract_data(data_dict, dataset):
    _debug("Plugin {plugin}: extracting dataset metrics {metrics!r} from dict {data_dict!r}".format(
        plugin=PLUGIN_NAME, metrics=[d[0] for d in dataset], data_dict=data_dict))
    return [_type_cast(data_dict[ds_name], float if ds_type == collectd.DS_TYPE_GAUGE else int, ds_min)
            for ds_name, ds_type, ds_min, ds_max in dataset]


def _parse_dataset(type_instance_list, dataset, meta_keys=None):
    _debug("Plugin {plugin}: parsing {m} type metrics for each type instance in {n}, meta_keys: {meta_keys!r}".format(
        plugin=PLUGIN_NAME, m=len(dataset), n=len(type_instance_list), meta_keys=meta_keys))
    ds_names = [ds[0] for ds in dataset]
    for instance_dict in type_instance_list:
        data = _extract_data(instance_dict, dataset)
        if meta_keys is None:
            metadata = dict([(k, v) for k, v in instance_dict.items() if k not in ds_names])
        else:
            metadata = dict([(k, instance_dict[k]) for k in meta_keys if k in instance_dict])
        yield data, metadata


def _read_one_command(val, url, command, type_instance_key, client_command, extra_metadata=None, meta_keys=None):
    _debug("Plugin {plugin}: reading metrics for {command}".format(plugin=PLUGIN_NAME, command=command))
    dataset_name = "eos_" + command
    command_string = command + " ls -m"
    instance_list = [_parse_em_line(line) for line in _eos_command(url, command_string, client_command)]
    _debug("Plugin {plugin}: got {n} results from {command}"
           .format(plugin=PLUGIN_NAME, n=len(instance_list), command=command))
    _debug("Plugin {plugin}: getting dataset {dataset!r} type definition"
           .format(plugin=PLUGIN_NAME, dataset=dataset_name))
    dataset = collectd.get_dataset(dataset_name)
    for data, metadata in _parse_dataset(instance_list, dataset, meta_keys):
        if metadata is not None:
            metadata.update(extra_metadata)
        val.dispatch(type=dataset_name, values=data, type_instance=metadata[type_instance_key], meta=metadata)


def read_callback(data):
    name, url, interval, client_command = data
    _debug("Plugin {plugin}, instance {instance!r}: read callback with "
           "url {url!r}, interval {interval!r} and client command {command!r}"
           .format(plugin=PLUGIN_NAME, instance=name, url=url, interval=interval, command=client_command))
    val = collectd.Values(plugin=PLUGIN_NAME, plugin_instance=name)
    if interval > 0:
        val.interval = interval
    _debug("Plugin {plugin}, instance {instance!r}: getting version metadata".format(plugin=PLUGIN_NAME, instance=name))
    version_lines = _eos_command(url, "version -m", client_command)
    version_combined_line = " ".join(version_lines)
    version_data = _parse_em_line(version_combined_line)
    _debug("Plugin {plugin}, instance {instance!r}: reading node data".format(plugin=PLUGIN_NAME, instance=name))
    _read_one_command(val, url, "node", "hostport", client_command, version_data)
    _debug("Plugin {plugin}, instance {instance!r}: reading fs data".format(plugin=PLUGIN_NAME, instance=name))
    _read_one_command(val, url, "fs", "id", client_command, version_data)
    _debug("Plugin {plugin}, instance {instance!r}: reading space data".format(plugin=PLUGIN_NAME, instance=name))
    _read_one_command(val, url, "space", "name", client_command, version_data)
    _debug("Plugin {plugin}, instance {instance!r}: reading recycle data".format(plugin=PLUGIN_NAME, instance=name))
    _read_one_command(val, url, "recycle", "recycle-bin", client_command, version_data)


def _parse_config_node(conf):
    for node in conf.children:
        if node.children:
            collectd.warning("Plugin {plugin}: unexpected config block {block!r}, ignoring"
                             .format(plugin=PLUGIN_NAME, block=node.key))
            continue
        yield node.key.lower(), node.values


def _parse_config_keys(key_value_list):
    instances = []
    intervals = []
    commands = []
    for key, values in key_value_list:
        if key == "instance":
            instances.append(values)
        elif key == "interval":
            intervals.append(values)
        elif key == "eos_client_command":
            commands.append(values)
        else:
            collectd.warning("Plugin {plugin}: unexpected config key: {key!r}, ignoring"
                             .format(plugin=PLUGIN_NAME, key=key))
    return instances, intervals, commands


def _check_instance(values):
    if len(values) != 2:
        raise collectd.CollectdError("Plugin {plugin}: instance expects 2 values: instance_name, mgm_url; "
                                     "found values: {values!r}".
                                     format(plugin=PLUGIN_NAME, values=values))
    return values


def _check_instances(instances):
    _debug("Plugin {plugin}: configuring monitored instances".format(plugin=PLUGIN_NAME))
    url_count = defaultdict(int)
    name_count = defaultdict(int)
    for name, url in instances:
        name_count[name] += 1
        url_count[url] += 1
        if name_count[name] > 1:
            raise collectd.CollectdError("Plugin {plugin}: plugin instance names must be unique, found reused: {name!r}"
                                         .format(plugin=PLUGIN_NAME, name=name))
        if url_count[url] > 1:
            collectd.warning("Plugin {plugin}: configuring instance {instance!r} mgm_url {url!r}, "
                             "URL already used in another instance"
                             .format(plugin=PLUGIN_NAME, instance=name, url=url))
    return instances


def _parse_interval(intervals):
    _debug("Plugin {plugin}: configuring interval".format(plugin=PLUGIN_NAME))
    if len(intervals) == 0:
        _debug("Plugin {plugin}: using default interval".format(plugin=PLUGIN_NAME))
        return CONFIG_DEFAULT_INTERVAL
    elif len(intervals) == 1:
        _debug("Plugin {plugin}: configuring custom interval".format(plugin=PLUGIN_NAME))
        val = intervals[0]
        if len(val) != 1:
            raise collectd.CollectdError("Plugin {plugin}: interval key expects 1 value, found: {n}"
                                         .format(plugin=PLUGIN_NAME, n=val))
        _debug("Plugin {plugin}: configured custom interval: {interval!r}"
               .format(plugin=PLUGIN_NAME, interval=intervals[0][0]))
        return intervals[0][0]
    else:
        raise collectd.CollectdError(
            "Plugin {plugin}: found {n} interval keys in config, expecting no more than one".format(
                plugin=PLUGIN_NAME, n=len(intervals)))


def _parse_client_command(commands):
    _debug("Plugin {plugin}: configuring eos client command".format(plugin=PLUGIN_NAME))
    if len(commands) == 0:
        _debug("Plugin {plugin}: client command not customized, using default eos command: {command!r}"
               .format(plugin=PLUGIN_NAME, command=CONFIG_DEFAULT_EOS_CLIENT_COMMAND))
        return CONFIG_DEFAULT_EOS_CLIENT_COMMAND
    elif len(commands) == 1:
        _debug("Plugin {plugin}: configuring custom eos client command".format(plugin=PLUGIN_NAME))
        val = commands[0]
        if not val:
            raise collectd.CollectdError("Plugin {plugin}: config key eos_client_command must not be empty".
                                         format(plugin=PLUGIN_NAME))
        _debug("Plugin {plugin}: custom eos client command configured: {command!r}"
               .format(plugin=PLUGIN_NAME, command=val))
        return val
    else:
        raise collectd.CollectdError(
            "Plugin {plugin}: found {n} eos_client_command keys in config, expecting no more than one".format(
                plugin=PLUGIN_NAME, n=len(commands)))


def _get_default_instance():
    _debug("Plugin {plugin}: configuring default instance".format(plugin=PLUGIN_NAME))
    if "EOS_MGM_URL" in os.environ:
        _debug("Plugin {plugin}: using $EOS_MGM_URL to configure default instance".format(plugin=PLUGIN_NAME))
        url = os.environ["EOS_MGM_URL"]
        name = urlparse(url).hostname
        if name is None:
            collectd.warning("Plugin {plugin}: failed to extract instance hostname from $EOS_MGM_URL: {url!r}".format(
                plugin=PLUGIN_NAME, url=url))
            name = url
    else:
        _debug("Plugin {plugin}: using hostname to configure default instance".format(plugin=PLUGIN_NAME))
        name = socket.getfqdn()
        url = "root://" + name
    _debug("Plugin {plugin}: using default instance: {instance!r}, url: {url!r}"
           .format(plugin=PLUGIN_NAME, instance=name, url=url))
    return name, url


def configure_callback(conf):
    config = _parse_config_node(conf)
    instance_vals, interval_vals, command_vals = _parse_config_keys(config)
    instances = _check_instances([_check_instance(vals) for vals in instance_vals])
    if not instances:
        instances = [_get_default_instance()]
    interval = _parse_interval(interval_vals)
    client_command = _parse_client_command(command_vals)
    for name, url in instances:
        collectd.info("Plugin {plugin}: configured instance {instance!r} with mgm_url={url!r}, "
                      "interval={interval!r}, eos_client_command={command!r}".format(
            plugin=PLUGIN_NAME, instance=name, url=url, interval=interval, command=client_command))
        kwargs = dict(callback=read_callback, data=(name, url, interval, client_command),
                      name=name)
        if interval > 0:
            kwargs["interval"] = interval
        collectd.register_read(**kwargs)


collectd.register_config(configure_callback)
