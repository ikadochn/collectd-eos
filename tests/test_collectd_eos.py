import sys

import pytest
import subprocess
from mock import Mock, patch, call


class CollectdError(Exception):
    pass


@pytest.fixture
def collectd_eos():
    collectd = Mock(spec=["register_config", "register_read", "get_dataset", "Values",
                          "debug", "info", "warning", "error"],
                    CollectdError=CollectdError)
    with patch.dict(sys.modules, {"collectd": collectd}):
        import collectd_eos
        collectd_eos._debug = Mock(spec=())
        yield collectd_eos


def test_parse_em_line(collectd_eos):
    assert collectd_eos._parse_em_line(
        "a=b number=123    this=that") == {"a": "b", "number": "123", "this": "that"}
    assert collectd_eos._debug.mock_calls == [
        call("Plugin eos: parsing metrics line 'a=b number=123    this=that'")]


def test_eos_command(collectd_eos, mocker):
    mocker.patch("subprocess.Popen", autospec=True,
                 return_value=mocker.Mock(spec=["communicate"],
                                          communicate=mocker.Mock(
                                              return_value=("process stdout\n \n\n a=b\nc=d  ", "some error"))))
    assert collectd_eos._eos_command("root://eos.example", "command arg1 -arg2", ("client", "eos")) == [
        "process stdout", " a=b", "c=d  "]
    assert subprocess.Popen.mock_calls == [
        call(("client", "eos", "-b", "-r", "0", "0", "root://eos.example", "command arg1 -arg2"),
             stdout=subprocess.PIPE, stderr=subprocess.PIPE)]
    assert collectd_eos._debug.mock_calls == [
        call("Plugin eos: running eos client command 'client eos -b -r 0 0 root://eos.example command arg1 -arg2'")]
    assert collectd_eos.collectd.error.mock_calls == [
        call("Plugin eos: error when running command 'command arg1 -arg2':\nsome error")]


def test_type_cast(collectd_eos):
    assert collectd_eos._type_cast("13", int) == 13
    assert collectd_eos._type_cast("13", float, 0) == 13
    assert collectd_eos._type_cast("13.4", int) == -1
    assert collectd_eos._type_cast("13.4", float) == 13.4
    assert collectd_eos._type_cast("1-3", int, 0) == -1
    assert collectd_eos._type_cast("1-3", float) == -1
    assert collectd_eos._type_cast("1-3", int, 4) == 3
    assert collectd_eos._type_cast("13.%", int) == -1
    assert collectd_eos._type_cast("13a%", float) == -1
    assert collectd_eos._type_cast("-13%", int) == -13
    assert collectd_eos._type_cast("-13%", float) == -13


def test_extract_data(collectd_eos):
    data = {"a": "1", "b": "2", "c": "3"}
    gauge = collectd_eos.collectd.DS_TYPE_GAUGE = 1
    counter = collectd_eos.collectd.DS_TYPE_COUNTER = 2
    dataset = [("a", gauge, 0, None), ("b", counter, None, 100)]
    assert collectd_eos._extract_data(data, dataset) == [1.0, 2]
    data = {"a": "1.2", "b": "2.2", "c": "3.2"}
    assert collectd_eos._extract_data(data, dataset) == [1.2, -1]
    data = {"a": "1", "c": "3"}
    with pytest.raises(KeyError):
        collectd_eos._extract_data(data, dataset)
    assert collectd_eos._debug.mock_calls == [
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1', 'b': '2', 'c': '3'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1.2', 'b': '2.2', 'c': '3.2'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1', 'c': '3'}))]


def test_parse_dataset(collectd_eos):
    instance_list = [{"a": "1", "b": "2", "c": "3"},
                     {"a": "1.2", "b": "2.2", "c": "3.2"}]
    gauge = collectd_eos.collectd.DS_TYPE_GAUGE = 1
    counter = collectd_eos.collectd.DS_TYPE_COUNTER = 2
    dataset = [("a", gauge, 0, None), ("b", counter, None, 100)]
    assert list(collectd_eos._parse_dataset(instance_list, dataset)) == [([1.0, 2], {"c": "3"}),
                                                                         ([1.2, -1], {"c": "3.2"})]
    assert list(collectd_eos._parse_dataset(instance_list, dataset, set("b"))) == [([1.0, 2], {"b": "2"}),
                                                                                   ([1.2, -1], {"b": "2.2"})]
    assert collectd_eos._debug.mock_calls == [
        call("Plugin eos: parsing 2 type metrics for each type instance in 2, meta_keys: None"),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1', 'b': '2', 'c': '3'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1.2', 'b': '2.2', 'c': '3.2'})),
        call("Plugin eos: parsing 2 type metrics for each type instance in 2, meta_keys: {0}".format({'b'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1', 'b': '2', 'c': '3'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {'a': '1.2', 'b': '2.2', 'c': '3.2'}))]


def test_read_one_command(collectd_eos, mocker):
    mocker.patch("subprocess.Popen", autospec=True,
                 return_value=mocker.Mock(spec=["communicate"],
                                          communicate=mocker.Mock(
                                              return_value=("a=1.5 b=3 name=asdf  \n name=zzz a=2 b=4\n", ""))))
    gauge = collectd_eos.collectd.DS_TYPE_GAUGE = 1
    counter = collectd_eos.collectd.DS_TYPE_COUNTER = 2
    val = mocker.Mock(spec=["dispatch"])
    collectd_eos.collectd.get_dataset = mocker.Mock(return_value=([("a", gauge, 0, 100),
                                                                   ("b", counter, 0, None)]))
    collectd_eos._read_one_command(val, "root://test.example", "cmd", type_instance_key="name",
                                   client_command=("client", "eos"), extra_metadata={"version": "13"})
    assert subprocess.Popen.mock_calls == [call(("client", "eos", "-b", "-r", "0", "0", "root://test.example",
                                                 "cmd ls -m"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)]
    collectd_eos.collectd.get_dataset.assert_called_once_with("eos_cmd")
    assert val.mock_calls == [call.dispatch(type="eos_cmd", values=[1.5, 3], type_instance="asdf",
                                            meta={"version": "13", "name": "asdf"}),
                              call.dispatch(type="eos_cmd", values=[2.0, 4], type_instance="zzz",
                                            meta={"version": "13", "name": "zzz"})]
    assert collectd_eos._debug.mock_calls == [
        call("Plugin eos: reading metrics for cmd"),
        call("Plugin eos: running eos client command 'client eos -b -r 0 0 root://test.example cmd ls -m'"),
        call("Plugin eos: parsing metrics line {0!r}".format(u"a=1.5 b=3 name=asdf  ")),
        call("Plugin eos: parsing metrics line {0!r}".format(u" name=zzz a=2 b=4")),
        call("Plugin eos: got 2 results from cmd"),
        call("Plugin eos: getting dataset 'eos_cmd' type definition"),
        call("Plugin eos: parsing 2 type metrics for each type instance in 2, meta_keys: None"),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {u'a': u'1.5', u'b': u'3', u'name': u'asdf'})),
        call("Plugin eos: extracting dataset metrics ['a', 'b'] from dict {0}".format(
            {u'name': u'zzz', u'a': u'2', u'b': u'4'}))]


def test_read_callback(collectd_eos, mocker):
    val = collectd_eos.collectd.Values = Mock(spec=["dispatch"])
    gauge = collectd_eos.collectd.DS_TYPE_GAUGE = 1
    counter = collectd_eos.collectd.DS_TYPE_COUNTER = 2
    mocker.patch("subprocess.Popen", autospec=True,
                 return_value=mocker.Mock(
                     spec=["communicate"],
                     communicate=mocker.Mock(
                         return_value=("a=1 b=2 c=3 name=asdf hostport=host:123 id=123 recycle-bin=/eos/recycle", ""))))
    collectd_eos.collectd.get_dataset = mocker.Mock(return_value=([("a", gauge, 0, 100),
                                                                   ("b", counter, 0, None)]))
    collectd_eos.read_callback(("test", "root://test.example", -1, ("client", "eos")))
    # a and b are coming into meta from version_data
    # mock Popen better for a more realistic test
    expected_meta = {"a": "1", "b": "2", "c": "3", "name": "asdf", "hostport": "host:123", "id": "123",
                     "recycle-bin": "/eos/recycle"}
    assert val.mock_calls == [call(plugin=collectd_eos.PLUGIN_NAME, plugin_instance="test"),
                              call().dispatch(type="eos_node", type_instance="host:123", values=[1.0, 2],
                                              meta=expected_meta),
                              call().dispatch(type="eos_fs", type_instance="123", values=[1.0, 2],
                                              meta=expected_meta),
                              call().dispatch(type="eos_space", type_instance="asdf", values=[1.0, 2],
                                              meta=expected_meta),
                              call().dispatch(type="eos_recycle", type_instance="/eos/recycle", values=[1.0, 2],
                                              meta=expected_meta),
                              ]
    collectd_eos._debug.assert_has_calls([
        call("Plugin eos, instance 'test': read callback with url 'root://test.example', "
             "interval -1 and client command ('client', 'eos')"),
        call("Plugin eos, instance 'test': getting version metadata"),
        call("Plugin eos, instance 'test': reading node data"),
        call("Plugin eos, instance 'test': reading fs data"),
        call("Plugin eos, instance 'test': reading space data"),
        call("Plugin eos, instance 'test': reading recycle data")
    ], any_order=True)


class MockCollectdConfig(object):
    def __init__(self, *args):
        self.children = [Mock(spec=[], key=k, values=v, children=()) for k, v in args]


def test_parse_config_node(collectd_eos):
    conf = MockCollectdConfig(("a", [1, 2, 3]),
                              ("B", [1]),
                              ("A", [4, 5]))
    assert list(collectd_eos._parse_config_node(conf)) == [("a", [1, 2, 3]), ("b", [1]), ("a", [4, 5])]
    conf.children[1].children = ("something",)
    assert list(collectd_eos._parse_config_node(conf)) == [("a", [1, 2, 3]), ("a", [4, 5])]
    collectd_eos.collectd.warning.assert_called_once_with("Plugin eos: unexpected config block 'B', ignoring")


def test_parse_config_keys(collectd_eos):
    parse = collectd_eos._parse_config_keys
    warning = collectd_eos.collectd.warning
    assert parse([]) == ([], [], [])
    warning.assert_not_called()
    assert parse([("A", (1,))]) == ([], [], [])
    assert warning.mock_calls == [call("Plugin eos: unexpected config key: 'A', ignoring")]
    warning.reset_mock()
    assert parse(
        [("instance", ("a", "b")), ("interval", [13])]) == ([("a", "b")], [[13]], [])
    warning.assert_not_called()
    assert parse(
        [("instance", "a"), ("instance", "bbb"), ("instance", "c")]) == (["a", "bbb", "c"], [], [])
    warning.assert_not_called()
    assert parse(
        [("instance", "a"), ("eos_client_command", ("client", "command"))]) == (["a"], [], [("client", "command")])
    warning.assert_not_called()
    assert parse(
        [("wrong", 123), ("instance", "a"), ("key", "value"),
         ("eos_client_command", ("client", "command")), ("instance_", "asdf")]) == (["a"], [], [("client", "command")])
    assert warning.mock_calls == [call("Plugin eos: unexpected config key: 'wrong', ignoring"),
                                  call("Plugin eos: unexpected config key: 'key', ignoring"),
                                  call("Plugin eos: unexpected config key: 'instance_', ignoring")]


def test_check_instance(collectd_eos):
    check = collectd_eos._check_instance
    CollectdError = collectd_eos.collectd.CollectdError
    assert check((1, 2)) == (1, 2)
    with pytest.raises(CollectdError,
                       match=r"Plugin eos: instance expects 2 values: instance_name, mgm_url; found values: \(\)"):
        check(())
    with pytest.raises(CollectdError,
                       match=r"Plugin eos: instance expects 2 values: instance_name, mgm_url; found values: \(1, 2, 3\)"):
        check((1, 2, 3))


def test_check_instances(collectd_eos):
    check = collectd_eos._check_instances
    warning = collectd_eos.collectd.warning
    debug = collectd_eos._debug
    CollectdError = collectd_eos.collectd.CollectdError

    assert check([]) == []
    warning.assert_not_called()
    assert debug.mock_calls == [call("Plugin eos: configuring monitored instances")]
    debug.reset_mock()

    instances = [("a", "url"), ("b", "different"), ("A", "other")]
    assert check(instances) == instances
    warning.assert_not_called()
    assert debug.mock_calls == [call("Plugin eos: configuring monitored instances")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match="Plugin eos: plugin instance names must be unique, found reused: 'a'"):
        instances = [("a", "url"), ("b", "different"), ("a", "url"), ("b", "url")]
        check(instances)
    warning.assert_not_called()
    assert debug.mock_calls == [call("Plugin eos: configuring monitored instances")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match="Plugin eos: plugin instance names must be unique, found reused: 'a'"):
        instances = [("a", "url"), ("b", "url"), ("c", "url"), ("a", "url")]
        check(instances)
    assert warning.mock_calls == [call("Plugin eos: configuring instance 'b' mgm_url 'url',"
                                       " URL already used in another instance"),
                                  call("Plugin eos: configuring instance 'c' mgm_url 'url',"
                                       " URL already used in another instance")]
    assert debug.mock_calls == [call("Plugin eos: configuring monitored instances")]


def test_parse_interval(collectd_eos):
    parse = collectd_eos._parse_interval
    debug = collectd_eos._debug
    CollectdError = collectd_eos.collectd.CollectdError
    assert parse([]) == collectd_eos.CONFIG_DEFAULT_INTERVAL
    assert debug.mock_calls == [call("Plugin eos: configuring interval"),
                                call("Plugin eos: using default interval")]
    debug.reset_mock()

    assert parse([(13,)]) == 13
    assert collectd_eos._debug.mock_calls == [call("Plugin eos: configuring interval"),
                                              call("Plugin eos: configuring custom interval"),
                                              call("Plugin eos: configured custom interval: 13")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match=r"Plugin eos: interval key expects 1 value, found: \(1, 1, 1\)"):
        parse([(1, 1, 1)])
    assert collectd_eos._debug.mock_calls == [call("Plugin eos: configuring interval"),
                                              call("Plugin eos: configuring custom interval")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match=r"Plugin eos: interval key expects 1 value, found: \(\)"):
        parse([()])
    assert collectd_eos._debug.mock_calls == [call("Plugin eos: configuring interval"),
                                              call("Plugin eos: configuring custom interval")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match="Plugin eos: found 3 interval keys in config, expecting no more than one"):
        parse([(13,), (13,), (13,)])
    assert collectd_eos._debug.mock_calls == [call("Plugin eos: configuring interval")]


def test_parse_client_command(collectd_eos):
    parse = collectd_eos._parse_client_command
    debug = collectd_eos._debug
    CollectdError = collectd_eos.collectd.CollectdError

    assert parse([]) == collectd_eos.CONFIG_DEFAULT_EOS_CLIENT_COMMAND
    assert debug.mock_calls == [call("Plugin eos: configuring eos client command"),
                                call("Plugin eos: client command not customized, using default eos command: ('eos',)")]
    debug.reset_mock()

    assert parse([("eos",)]) == ("eos",)
    assert debug.mock_calls == [call("Plugin eos: configuring eos client command"),
                                call("Plugin eos: configuring custom eos client command"),
                                call("Plugin eos: custom eos client command configured: ('eos',)")]
    debug.reset_mock()

    assert parse([("ssh", "client.hostname.example", "eos")]) == ("ssh", "client.hostname.example", "eos")
    assert debug.mock_calls == [call("Plugin eos: configuring eos client command"),
                                call("Plugin eos: configuring custom eos client command"),
                                call("Plugin eos: custom eos client command configured: "
                                     "('ssh', 'client.hostname.example', 'eos')")]
    debug.reset_mock()

    with pytest.raises(CollectdError, match="Plugin eos: config key eos_client_command must not be empty"):
        parse([()])
    assert debug.mock_calls == [call("Plugin eos: configuring eos client command"),
                                call("Plugin eos: configuring custom eos client command")]
    debug.reset_mock()

    with pytest.raises(CollectdError,
                       match="Plugin eos: found 3 eos_client_command keys in config, expecting no more than one"):
        parse([("eos",), ("eos2",), ("ssh", "host", "eos")])
    assert debug.mock_calls == [call("Plugin eos: configuring eos client command")]
    debug.reset_mock()


def test_get_default_instance(collectd_eos, mocker):
    get = collectd_eos._get_default_instance
    warning = collectd_eos.collectd.warning
    debug = collectd_eos._debug
    mocker.patch("socket.getfqdn", return_value="hostname.example")

    with patch.dict("os.environ", clear=True):
        assert get() == ("hostname.example", "root://hostname.example")
        warning.assert_not_called()
    with patch.dict("os.environ", {"EOS_MGM_URL": "root://hostname.from.env"}):
        assert get() == ("hostname.from.env", "root://hostname.from.env")
        warning.assert_not_called()
    with patch.dict("os.environ", {"EOS_MGM_URL": "http://hostname.from.env:1234/some/place"}):
        # probably ok not to check, eos client will fail later
        assert get() == ("hostname.from.env", "http://hostname.from.env:1234/some/place")
        warning.assert_not_called()
    with patch.dict("os.environ", {"EOS_MGM_URL": "invalid url"}):
        assert get() == ("invalid url", "invalid url")
        assert warning.mock_calls == [
            call("Plugin eos: failed to extract instance hostname from $EOS_MGM_URL: 'invalid url'")
        ]
    assert debug.mock_calls == [
        call("Plugin eos: configuring default instance"),
        call("Plugin eos: using hostname to configure default instance"),
        call("Plugin eos: using default instance: 'hostname.example', url: 'root://hostname.example'"),
        call("Plugin eos: configuring default instance"),
        call("Plugin eos: using $EOS_MGM_URL to configure default instance"),
        call("Plugin eos: using default instance: 'hostname.from.env', url: 'root://hostname.from.env'"),
        call("Plugin eos: configuring default instance"),
        call("Plugin eos: using $EOS_MGM_URL to configure default instance"),
        call("Plugin eos: using default instance:"
             " 'hostname.from.env', url: 'http://hostname.from.env:1234/some/place'"),
        call("Plugin eos: configuring default instance"),
        call("Plugin eos: using $EOS_MGM_URL to configure default instance"),
        call("Plugin eos: using default instance: 'invalid url', url: 'invalid url'")]


def test_plugin_registration(collectd_eos):
    collectd_eos.collectd.register_config.assert_called_once_with(collectd_eos.configure_callback)


def test_configure_callback(collectd_eos, mocker):
    register_read = collectd_eos.collectd.register_read
    collectd_eos.configure_callback(MockCollectdConfig(("instance", ("test", "root://test.example"))))
    assert register_read.mock_calls == [call(name="test",
                                             data=("test", "root://test.example", -1, ("eos",)),
                                             callback=collectd_eos.read_callback)]
    register_read.reset_mock()

    collectd_eos.configure_callback(MockCollectdConfig(("instance", ("a", "asdf")),
                                                       ("INSTANCE", ("z", "zxcv")),
                                                       ("intErVal", (60,))))
    assert register_read.mock_calls == [call(name="a",
                                             data=("a", "asdf", 60, ("eos",)),
                                             callback=collectd_eos.read_callback,
                                             interval=60,
                                             ),
                                        call(name="z",
                                             data=("z", "zxcv", 60, ("eos",)),
                                             callback=collectd_eos.read_callback,
                                             interval=60,
                                             )]
    register_read.reset_mock()

    mocker.patch("socket.getfqdn", return_value="hostname.example")
    collectd_eos.configure_callback(MockCollectdConfig(("interval", (60,)), ))
    assert register_read.mock_calls == [call(name="hostname.example",
                                             data=("hostname.example", "root://hostname.example", 60, ("eos",)),
                                             callback=collectd_eos.read_callback,
                                             interval=60)]
