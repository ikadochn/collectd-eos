%global gitlab https://gitlab.cern.ch/ikadochn/
%global package_name collectd_eos

Name:           collectd-eos
Version:        0.3.1
Release:        1%{?dist}
Summary:        Collectd plugin to monitor EOS MGM metrics

License:        ASL 2.0
URL:            %{gitlab}/%{name}
Source0:        %{gitlab}/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python2-devel
BuildRequires:  python-setuptools

%description
Collectd plugin to collect EOS_ metrics. Connects to a mgm
instance using eos client and publishes node, fs and space metrics.

%prep
%autosetup

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}
mkdir -p %{buildroot}%{_datarootdir}/collectd/
mkdir -p %{buildroot}%{_sysconfdir}/collectd.d/
install -m 0644 eos.types.db %{buildroot}%{_datarootdir}/collectd/eos.types.db
install -m 0644 eos_types.conf %{buildroot}%{_sysconfdir}/collectd.d/eos_types.conf

%files

%if 0%{?el6}
%doc LICENSE
%else
%license LICENSE
%endif

%doc README.rst CHANGELOG.rst collectd.conf
%config(noreplace) %{_datarootdir}/collectd/eos.types.db
%config(noreplace) %{_sysconfdir}/collectd.d/eos_types.conf
%{python_sitelib}/%{package_name}
%{python_sitelib}/%{package_name}-%{version}-py?.?.egg-info

%changelog

* Thu Mar 14 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.3.1-1
- Version 0.3.1

* Thu Mar 14 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.2.2-1
- Version 0.2.2

* Thu Mar 7 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.2.1-4
- Install eos_types.conf into /etc/collectd.d (fixed!)

* Thu Mar 7 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.2.1-3
- Install eos.types.db into /usr/share/collectd
- Install eos_types.conf into /etc/collect.d

* Thu Mar 7 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.2.1-2
- Install eos.types.db and collectd.conf as docs.

* Thu Feb 28 2019 Ivan Kadochnikov <ivan.kadochikov@cern.ch> - 0.2.1-1
- Initial package.
